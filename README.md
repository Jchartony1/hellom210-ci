# hellom210-ci Beispiel

- Erstelle dir ein neues GitLab Projekt
- Kopiere die Inhalte von diesem Projekt in dein Projekt
- Verwende die Inhalte vom Projekt hellom210-docker als Grundlage
- Denke daran die Namen und Pfade an dein GitLab Projekt anzupassen

## .gitlab-ci.yml Bemerkungen

- Deaktivere vorerst die Stages Test und Deploy. Dazu einfach die Zeile im stages: Block auskommentieren mit einem #. Es ist wichtig, dass du deine Konfiguration Schritt-für-Schritt aufbaust.

- Im Block variables: werden einige Variabeln definiert damit wiederkehrende Informationen nicht wiederholt werden müssen. Passe diese an dein Projekt an.

- Damit wir nicht in jedem Schrit ein Image erstellen müssen und wir das Image erst in die Container-Registry speichern möchten wenn ein Test erfolgreich ist verwenden wir sogenannte Artefakte. Artefakte erlauben es Dateien zu cachen damit sie in folgenen Stages zu Vefügung stehen. Jeder Build-Stage verwendet quasi wieder eine neue Umgebung und ist nur temporär, verliert also alle gespeicherten Dateien und Konfigurationen.

- Eine Standardkonvention zum speichern von Docker-Images ist wie folgend:
  registry.gitlab.com/coaching-m210/hellom210-ci/hellom210:1.0.0
  <REGISTRY_URL>/<IMAGE_PATH>/<IMAGE_NAME>:<IMAGE_VERSION>
